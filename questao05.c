/*
  @author: Nabson Paiva
  @schoolSubject: AEDI - Ciência da Computação
  @question: Faça uma função que receba como parâmetro um vetor deinteiros
    (V) e retorne verdadeiro caso o vetor esteja ordenado de forma crescente
    (V[i] <= V[j] se i<j). (P 2006)
*/

int ordenado(int v[], int n) {
  for (int i=0; i<n-1; i++) {
    if (v[i] > v[i+1])
    return 0;
  }
  return 1;
}
