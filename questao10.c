/*
  @author: Nabson Paiva
  @schoolSubject: AEDI - Ciência da Computação
  @question: Implemente uma função que receba um vetor ordenado de
    inteiros e uma chave, também inteira. A função deve retornar
    verdadeiro caso a chave ocorra exatamente 1 vez no vetor,
    retornando falso em caso contrário. Considere que o vetor pode
    conter repetições de valores. Utilize busca binária para
    implementar a solução para o problema fornecido.
*/

int ocorreUmaVez(int chv, int v[], int t) {
  int c = 0, f = t-1;
  int medio;
  while (c <= f) {
    medio = (f+c)/2;
    if (chv > v[medio])
      c = medio + 1;
    else if (chv < v[medio])
      f = medio - 1;
    else
      if ((medio<t && v[medio+1] == chv) || (medio > 0 && v[medio-1] == chv))
        return 0;
      else
        return 1;
  }
  return 0;
}
