/*
  @author: Nabson Paiva
  @schoolSubject: AEDI - Ciência da Computação
  @question: Faça uma função para que receba como parâmetro um vetor
    de inteiros com n elementos e retorne o desvio padrão dos valores
    do vetor. Encontrar a fórmula para o desvio padrão faz parte do
    exercício.
*/

#include <math.h>

float desvioPadrao(int v[], int n) {
  float media = 0.0;
  int i;
  for (i=0; i<n; i++) {
    media += v[i];
  }
  media /= n;
  float dp = 0.0;
  for (i=0; i<n; i++) {
    dp += pow(v[i] - media, 2);
  }
  dp /= n;
  return sqrt(dp);
}
