/*
  @author: Nabson Paiva
  @schoolSubject: AEDI - Ciência da Computação
  @question: Faça uma função para inverter os efeitos da função
    desenvolvida na questão anterior.
*/
//OBS.: a função atualiza o vetor com relação ao índice.

void inverteEfeitos(int v[], int t) {
  for (int i=1; i<t; i++) {
    v[i] += i-1;
  }
}
