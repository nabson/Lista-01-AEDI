/*
  @author: Nabson Paiva
  @schoolSubject: AEDI - Ciência da Computação
  @question: Faça uma função que receba como parâmetros um nome e uma
    letra. A função deve retornar a quantidade de vezes que a letra
    passada como parâmetro ocorre no nome. Durante a contagem
    considere que letras maiúsculas e minúsculas são iguais.
*/

int repeticoes(char l, char nome[]) {
  int i = 0, c = 0;
  while (nome[i]) {
    if (nome[i] == l || nome[i] == (l+32) || nome[i] == (l-32))
      c++;
    i++;
  }
  return c;
}
