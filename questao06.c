/*
  @author: Nabson Paiva
  @schoolSubject: AEDI - Ciência da Computação
  @question: Implemente o algoritmo de ordenação por inserção em uma
    função que receba como parâmetros um vetor de inteiros e seu
    tamanho. A implementação deve ser feita sem a utilização do
    comando “for” da linguagem C.
*/

void insertionSort(int v[], int n) {
  int j, i = 1;
  int pivo;
  while (i < n) {
    pivo = v[i];
    j = i - 1;
    while (j >= 0 && pivo < v[j]){
      v[j+1] = v[j];
      j--;
    }
    v[j+1] = pivo;
    i++;
  }
}
