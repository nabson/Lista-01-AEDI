/*
  @author: Nabson Paiva
  @schoolSubject: AEDI - Ciência da Computação
  @question: Faça uma função que receba como parâmetro um texto,
    armazenado em um vetor de caracteres de 40 posições e retorne
    verdadeiro caso o texto contenha a sequência de caracteres
    “UFAM” em posições contíguas. Ex: “eu estudo na UFAM,
    Universidade Federal do Amazonas” passado como parâmetro
    retornaria verdadeiro. (P 2006)
*/

int txtUFAM(char v[]) {
  char busca[] = "UFAM";
  for (int i=0, j=0; i<40; i++) {
    if (v[i] == busca[j]){
      j++;
      if (j == 4)
        return 1;
    } else {
      j = 0;
      if (v[i] == busca[j])
        j++;
    }
  }
  return 0;
}
