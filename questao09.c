/*
  @author: Nabson Paiva
  @schoolSubject: AEDI - Ciência da Computação
  @question: Faça uma função que receba como parâmetro um nome e
    retorne verdadeiro caso o nome seja um palíndromo, retornando
    falso em caso contrário.
*/

int palindromo(char nome[], int t) {
  for (int i=0, j=t-1; i < j; i++, j--) {
    if (nome[i] != nome[j])
      return 0;
  }
  return 1;
}
