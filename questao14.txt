14) Dado um vetor ordenado de inteiros com os valores: 3,12, 20,
    25, 40, 50, 55,90,95 e 99. Com o primeiro elemento armazenado
    na posição 0 e os demais em posições sucessivas. Dado ainda que
    as buscas por elementos neste vetor são realizadas utilizando-se o
    algoritmo de busca binária, indique que elementos seriam
    comparados com as seguintes chaves de busca, apresentando os
    elementos na ordem em que seriam comparados em cada caso: (P
    2005)
      a) 4
        R= 40, 12, 3.

      b) 55
        R= 40, 90, 50, 55.
