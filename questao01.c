/*
  @author: Nabson Paiva
  @schoolSubject: AEDI - Ciência da Computação
  @question: Faça uma função que calcule e retorne o produto dos n
    primeiros números positivos, onde n deve ser passado como
    parâmetro. Ex.: para n=4, a função deve retornar 4*3*2*1=24. Este
    valor é conhecido como fatorial de n. Se n não for positivo, a
    função deve retornar 1. (P 2005)
*/

int fatorial(int n){
  if (n < 0)
    return 1;
  else {
    int r = 1;
    while (n) {
      r *= n;
      n--;
    }
    return r;
  }
}
