/*
  @author: Nabson Paiva
  @schoolSubject: AEDI - Ciência da Computação
  @question: Faça uma função que receba como parâmetro um vetor de
    inteiros, o número de elementos do vetor e um parâmetro de
    referencia de valor inteiro delta. A função deve substituir cada
    elemento do vetor pela diferença entre seu valor original e delta.
*/

void atualiza(int v[], int t, int delta) {
  for (int i=0; i<t; i++) {
    v[i] -= delta;
  }
}
