/*
  @author: Nabson Paiva
  @schoolSubject: AEDI - Ciência da Computação
  @question: Faça uma função que receba como parâmetro um vetor com n
    inteiros e inverta as posições dos elementos de maneira que o
    primeiro elemento troque de valor com o último, o segundo com o
    penúltimo e assim por diante. (inverter as posições dos elementos
    do vetor)
*/

void inverte(int n, int v[]) {
  int aux;
  for (int i=n-1, j=0; i > j; i--, j++) {
    aux = v[i];
    v[i] = v[j];
    v[j] = aux;
  }
}
