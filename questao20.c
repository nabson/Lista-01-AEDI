/*
  @author: Nabson Paiva
  @schoolSubject: AEDI - Ciência da Computação
  @question: Faça uma função para calcular o n-ésimo número da
    sequencia de Fibonacci. A função deve retornar o valor do n-ésimo
    número, realizando a operação sem utilizar recursividade.
*/

int nEsimoFib(int n) {
  int ant1 = 1, ant2 = 0, x;
  for (int i=3; i<=n; i++) {
    x = ant1 + ant2;
    ant2 = ant1;
    ant1 = x;
  }
  return x;
}
