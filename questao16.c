/*
  @author: Nabson Paiva
  @schoolSubject: AEDI - Ciência da Computação
  @question: Faça uma função que receba como parâmetro uma matriz
    quadrada NxN de números inteiros, onde N é uma constante
    previamente definida, e retorne como resultado o maior elemento
    da matriz. (P 2005)
*/

#define n 2

int maiorElemento(int v[n][n]) {
  int m = v[0][0];
  for (int i=0; i<n; i++) {
    for (int j=0; j<n; j++) {
      if (m < v[i][j])
        m = v[i][j];
    }
  }
  return m;
}
