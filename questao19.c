/*
  @author: Nabson Paiva
  @schoolSubject: AEDI - Ciência da Computação
  @question: Faça uma função que receba como parâmetro um vetor nãoordenado e
    retorne a mediana dos valores do vetor, fazendo tal
    operação sem ordenar, copiar ou alterar o vetor. Considere que a
    quantidade de elementos é um valor ímpar. Lembrando que a
    mediana entre n valores é um valor que é ao mesmo tempo maior e
    menor que k números.
*/

int mediana(int v[], int n) {
  int men = 0, mai = 0, ig = 0;
  for (int i=0; i<n; i++) {
    for (int j=0; j<n; j++) {
      if (v[i] > v[j])
        mai++;
      else if (v[i] < v[j])
        men++;
      else if (i != j)
        ig++;
    }
    if (mai == men || (mai + ig) == men || (men + ig) == mai)
      return v[i];
    else {
      mai = 0;
      men = 0;
      ig = 0;
    }
  }
}
