/*
  @author: Nabson Paiva
  @schoolSubject: AEDI - Ciência da Computação
  @question: Faça uma função que calcule a soma dos elementos da
    diagonal secundária de uma matriz de inteiros passada como
    parâmetro e retorne esta soma como resultado. A dimensão da
    matriz deve ser NxN, onde N é uma constante. (P 2005)
*/

#define n 2

int somaDiagSec(int v[n][n]) {
  int s = 0;
  for (int i=0; i<n; i++) {
    for (int j=0; j<n; j++) {
      if (i + j + 1 == n)
        s += v[i][j];
    }
  }
  return s;
}
