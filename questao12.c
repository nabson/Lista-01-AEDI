/*
  @author: Nabson Paiva
  @schoolSubject: AEDI - Ciência da Computação
  @question: Faça uma função que receba como parâmetro um vetor de
    inteiros ordenado de forma crescente e o número de elementos do
    vetor. A função deve substituir cada elemento do vetor pela
    diferença entre seu valor original e o valor da posiçãoimediatamente
    anterior a ele, mantendo apenas o primeiro elemento com seu valor original.
*/
//OBS.: a função atualiza o vetor com relação ao índice.
void atualiza(int v[], int t) {
  for (int i=1; i<t; i++) {
    v[i] -= i-1;
  }
}
