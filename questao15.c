/*
  @author: Nabson Paiva
  @schoolSubject: AEDI - Ciência da Computação
  @question: Faça uma função que receba como parâmetro uma matriz
    quadrada NxN de números inteiros, onde N é uma constante
    previamente definida, e um número inteiro K. A função deve
    retornar verdadeiro caso a matriz contenha exatamente quatro
    posições com valor K e falso em caso contrário. (P 2006)
*/

#define n 3

int acontece4Vezes(int v[n][n], int k) {
  char c = 0;
  for (int i=0; i<n; i++) {
    for (int j=0; j<n; j++) {
      if (v[i][j] == k) {
        c++;
      }
    }
  }
  if (c == 4)
    return 1;
  else
    return 0;
}
