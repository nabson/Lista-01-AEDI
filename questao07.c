/*
  @author: Nabson Paiva
  @schoolSubject: AEDI - Ciência da Computação
  @question: Faça uma função que receba como parâmetro um inteiro N e
    retorne a soma dos quadrados de todos os números inteiros
    positivos de valor menor ou igual a N. Calcule iterativamente , sem
    usar a fórmula para calcular o valor. (P 2006)
*/

int quadrados( int n) {
  int r = 0;
  while (n) {
    r += n*n;
    n--;
  }
  return r;
}
